﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace oop___21
{
    public class Car
    {
        public string Model { get; set; }
        public string Brand { get; set; }
        public int Year { get; set; }
        public string Color { get; set; }
        private int _codan;
        protected int _numberOfSeats;
        public int GetCodan()
        {
            return _codan;
        }
        public int GetNumberOfSeats()
        {
            return _numberOfSeats;
        }
        protected void ChangeNumberOfSeats(int NewNumberOfSeats)
        {
            _numberOfSeats = NewNumberOfSeats;
        }

        public Car()
        {

        }
        public Car(string fileName)
        {
            XmlSerializer serializar = new XmlSerializer(typeof(Car));
            using (Stream file = new FileStream(fileName, FileMode.Open))
            {
                Car c;
                c = serializar.Deserialize(file) as Car;
                Model = c.Model;
                Brand = c.Brand;
                Year = c.Year;
                Color = c.Color;
                _codan = c._codan;
                _numberOfSeats = c._numberOfSeats;

            }
        }

        public Car(string model, string brand, int year, string color, int codan, int numberOfSeats)
        {
            Model = model;
            Brand = brand;
            Year = year;
            Color = color;
            _codan = codan;
            _numberOfSeats = numberOfSeats;
        }
        public static void SerializeACar(string fileName, Car c)
        {
            using (Stream file = new FileStream(fileName, FileMode.Create))
            {
                XmlSerializer serializar = new XmlSerializer(typeof(Car));
                serializar.Serialize(file, c);
            }
        }
        public static void SerializeCarArray(string fileName, Car[] c)
        {
            using (Stream file = new FileStream(fileName, FileMode.Create))
            {
                XmlSerializer serializar = new XmlSerializer(typeof(Car[]));
                serializar.Serialize(file, c);
            }
        }
        public static Car DeSerializeACar(string fileName)
        {
            using (Stream file = new FileStream(fileName, FileMode.Open))
            {
                XmlSerializer serializar = new XmlSerializer(typeof(Car));
                Car c = serializar.Deserialize(file) as Car;
                return c;
            }
        }
        public static Car[] DeSerializeCarArray(string fileName)
        {
            using (Stream file = new FileStream(fileName, FileMode.Open))
            {
                XmlSerializer serializar = new XmlSerializer(typeof(Car[]));
                Car[] c = serializar.Deserialize(file) as Car[];
                return c;
            }
        }
        public bool CarCompare(string fileName)
        {
            XmlSerializer serializar = new XmlSerializer(typeof(Car));
            using (Stream file = new FileStream(fileName, FileMode.Open))
            {
                Car c = serializar.Deserialize(file) as Car;
                return c.Model == Model && c.Year == Year && c.Brand == Brand && c.Color == Color &&
                c._numberOfSeats == _numberOfSeats && c._codan == _codan;
            }
        }

        public override string ToString()
        {
            return $"model: {Model}, brand: {Brand}, year: {Year}, color: {Color}";
        }
    }
}
