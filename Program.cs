﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace oop___21
{
    public class Program
    {
        static void Main(string[] args)
        {

            Car honda = new Car();
            Car.SerializeACar(@"..\..\Car.xml", honda);
            Car c = Car.DeSerializeACar(@"..\..\Car.xml");

            Car[] cars =
 {
                new Car ("bhg", "mazda", 2019, "brown", 7768, 5),
                new Car ("hh65", "suzuki", 2015, "grey", 7908, 5),
                new Car ("fj920", "lamburgini", 2017, "orange", 6539, 4)
             };
            Car.SerializeCarArray(@"..\..\Cars.xml", cars);
            Car[] carsArray = Car.DeSerializeCarArray(@"..\..\Cars.xml");

            Car carFromFile = new Car(@"..\..\Car.xml");
            Console.WriteLine(carFromFile.CarCompare(@"..\..\Car.xml"));


        }
    }
}
